
public class LinkedListApp {

	public static void main(String[] args) {
		
		LinkedList<Integer> list = new LinkedList<>();
		list.add(5);
		list.add(3);
		
		list.add(26548);
		list.insert(2, 587);
		
		list.remove(1);
		list.insert(2, 6777784);
		for(int i = 0; i < list.size(); ++i) {
			System.out.println(list.get(i));
		}
	}

}
