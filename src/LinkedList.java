import java.util.NoSuchElementException;

class LinkedList<T> {
	
	Node<T> first;
	
	public LinkedList() {
		first = null;
	}
	
	public void add(T data) {
		first = add(data, first);
	}
	
	private Node<T> add(T data, Node<T> n){
		if(n == null) {
			Node<T> temp = new Node<>();
			temp.data = data;
			temp.next = null;
			return temp;
		}
		else n.next = add(data, n.next);
		return n;
	}
	
	public T get(int index) {
		Node<T> current = first;
		for(int i = 0; i < index; i++) {
			current = current.next;
		}
		return current.data;
	}
	
	public void remove(int index) {
		remove(index, first);
	}
	
	private void remove(int index, Node<T> n) {
		if(n == null) throw new NoSuchElementException();
		if(index == 0) {
			first = first.next;
			return;
		}
		if(index == 1) {
			n.next = n.next.next;
			return;
		}
		else remove(index-1, n.next);
	}
	
	public void insert(int index, T data) {
		insert(index, data, first);
	}
	
	private void insert(int index, T data, Node<T> n) {
		if(index < 0 || index > size()) throw new NoSuchElementException();
		if(index == size()) {
			add(data);
			return;
		}
		
		if(index == 0) {
			Node<T> newNode = new Node<>();
			newNode.data = data;
			newNode.next = first;
			first = newNode;
			return;
		}
		
		if(index == 1) {
			Node<T> newNode = new Node<>();
			newNode.data = data;
			newNode.next = n.next;
			n.next = newNode;
			return;
		}
		else insert(index-1, data, first.next);
	}
	

	
	public boolean isEmpty() {
		return first == null;
				
	}
	
	public int size() {
		return size(first);
	}
	
	public int size(Node<T> n) {
	return	n == null ? 0 : (1+ size(n.next));
	
	}

}
